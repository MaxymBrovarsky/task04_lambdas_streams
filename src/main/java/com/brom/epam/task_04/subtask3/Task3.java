package com.brom.epam.task_04.subtask3;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task3 {
  private static final Logger logger = LogManager.getLogger(Task3.class.getName());
  private static final Scanner scanner = new Scanner(System.in);
  private static final String HELLO_MESSAGE =
      "Hello, please enter size(int) for generating list of integers";
  private static final String WRONG_INPUT_MESSAGE =
      "Please enter correct value";
  public static void main(String[] args) {
    logger.info(HELLO_MESSAGE);
    try {
      int size = scanner.nextInt();
      if (size < 1) {
        throw new InputMismatchException();
      }
      scanner.nextLine();
      List<Integer> generateRandomList = generateListOfRandomIntegersWithGenerate(size);
      List<Integer> iterateRandomList = randomWithIterate(size);
      List<Integer> rangeRandomList = randomWithRange(size);
      logger.info("List generated with generate");
      printStatistic(generateRandomList);
      logger.info("List generated with iterate");
      printStatistic(iterateRandomList);
      logger.info("List generated with range");
      printStatistic(rangeRandomList);
    } catch (InputMismatchException e) {
      logger.error(WRONG_INPUT_MESSAGE);
    }
  }

  private static List<Integer> generateListOfRandomIntegersWithGenerate(int size) {
    return Stream.generate(new Random()::nextInt)
        .limit(size)
        .collect(Collectors.toList());
  }

  private static List<Integer> randomWithIterate(int size) {
    Random random = new Random();
    int startValue = random.nextInt();
    int stepValue = random.nextInt();
    return generateListOfRandomIntegersWithIterate(size, startValue, stepValue);
  }
  private static List<Integer> generateListOfRandomIntegersWithIterate(int size, int start, int step) {
    return Stream.iterate(start, i -> i + step)
        .limit(size)
        .collect(Collectors.toList());
  }

  private static List<Integer> randomWithRange(int size) {
    Random random = new Random();
    int startValue = random.nextInt();
    int stopValue = random.nextInt();
    return generateListOfRandomIntegersWithRange(size, startValue, stopValue);
  }

  private static List<Integer> generateListOfRandomIntegersWithRange(int size, int start, int stop) {
    return IntStream.range(start, stop)
        .limit(size)
        .boxed()
        .collect(Collectors.toList());
  }

  private static void printStatistic(List<Integer> values) {
    String valuesString = values.stream()
        .map(i -> i.toString())
        .collect(Collectors.joining(", "));
    logger.info("Values of list: " + valuesString);
    int min = values.stream().min(Integer::compareTo).get();
    int max = values.stream().max(Integer::compareTo).get();
    int sum = values.stream().mapToInt(i -> i).sum();
    int reduceSum = values.stream().reduce(0,(a, b) -> a + b);
    logger.info("min = " + min);
    logger.info("max = " + max);
    logger.info("sum = " + sum);
    logger.info("reduceSum = " + reduceSum);
  }
}
