package com.brom.epam.task_04;

import com.brom.epam.task_04.subtask3.Task3;
import com.brom.epam.task_04.view.ConsoleView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
  private static final Logger logger = LogManager.getLogger(Main.class.getName());
  public static void main(String[] args) {
    new ConsoleView().show();
  }
}
