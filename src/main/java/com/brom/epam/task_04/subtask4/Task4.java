package com.brom.epam.task_04.subtask4;

import com.brom.epam.task_04.view.Command;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.PropertySource.Comparator;

public class Task4 implements Command {
  private static final Logger logger = LogManager.getLogger(Task4.class.getName());
  private static final Scanner scanner = new Scanner(System.in);
  private static final String HELLO_MESSAGE = "Hello, please enter text(ends with empty line)";
  @Override
  public void execute() {
    logger.info(HELLO_MESSAGE);
    String text = "";
    try {
      while (true) {
        String line = scanner.nextLine();
        if (line.equals("")) {
          break;
        }
        text += " " + line;
      }
      printStatistic(text);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  private void printStatistic(String inText) {
    String text = inText.replace(" ", "");
    Map<String,Long> wordToFrequency = Arrays.asList(text.split(" "))
        .stream()
        .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    long numberOfUniqueWords = wordToFrequency.entrySet().stream()
        .filter(e -> e.getValue() == 1)
        .count();
    List<String> uniqueWords = wordToFrequency.entrySet().stream()
        .filter(e -> e.getValue() == 1)
        .map(e -> e.getKey())
        .sorted()
        .collect(Collectors.toList());
    char[] chars = text.toCharArray();
    Map<Character, Long> charToFrequency = IntStream.range(0, chars.length)
        .mapToObj(i -> chars[i])
        .filter(c -> Character.isLowerCase(c))
        .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    logger.info("Number of unique words: " + numberOfUniqueWords);
    logger.info("Unique words: ");
    uniqueWords.forEach(w -> {
      logger.info(w);
    });
    logger.info("Words with frequency:");
    wordToFrequency.forEach((k, v) -> {
      logger.info(k + " - " + v);
    });
    logger.info("Lower case character with frequency:");
    charToFrequency.forEach((k, v) -> {
      logger.info(k + " - " + v);
    });
  }
}
