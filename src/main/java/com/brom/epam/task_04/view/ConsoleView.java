package com.brom.epam.task_04.view;

import com.brom.epam.task_04.subtask1.Task1;
import com.brom.epam.task_04.subtask3.Task3;
import com.brom.epam.task_04.subtask4.Task4;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView {
  private static final Logger logger = LogManager.getLogger(ConsoleView.class.getName());
  private static final Scanner scanner = new Scanner(System.in);
  private Map<String, Command> menu;

  public ConsoleView() {
    this.initMenu();
  }

  private void initMenu() {
    this.menu = new LinkedHashMap<>();
    this.menu.put("Task 1", () -> Task1.main(new String[0]));
    this.menu.put("Task 3", this::executeTask3);
    this.menu.put("Task 4", new Task4());
    this.menu.put("Quit", new Command() {
      @Override
      public void execute() {
        System.exit(0);
      }
    });
  }

  public void show() {
    while (true) {
      logger.info("Menu:");
      this.menu.keySet().forEach(k -> {
        logger.info(k);
      });
      logger.info("Enter Command:");
      String commandKey = scanner.nextLine();
      Optional<Command> command = Optional.ofNullable(this.menu.get(commandKey));
      if (command.isPresent()) {
        command.get().execute();
      }
    }
  }

  public void executeTask3() {
    Task3.main(new String[0]);
  }
}
