package com.brom.epam.task_04.view;

@FunctionalInterface
public interface Command {
  void execute();
}
