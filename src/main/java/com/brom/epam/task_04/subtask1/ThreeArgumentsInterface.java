package com.brom.epam.task_04.subtask1;

@FunctionalInterface
public interface ThreeArgumentsInterface {
  int apply(int a, int b, int c);
}
