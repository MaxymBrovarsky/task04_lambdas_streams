package com.brom.epam.task_04.subtask1;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task1 {
  private static final Logger logger = LogManager.getLogger(Task1.class.getName());
  private static final Scanner scanner = new Scanner(System.in);
  private static final String HELLO_MESSAGE = "Hello. This is program for testing lambdas."
      + "Please enter 3 integer values";
  private static final String WRONG_INPUT_MESSAGE = "Please enter correct values";
  public static void main(String[] args) {
    ThreeArgumentsInterface maxOfThreeNumbers =
        (a, b, c) -> Stream.of(a,b,c).max(Integer::compareTo).get();
    ThreeArgumentsInterface averageOfThreeNumbers = (a, b, c) -> (a + b + c) / 3;
    try {
      logger.info(HELLO_MESSAGE);
      int a = readValue();
      int b = readValue();
      int c = readValue();
      logger.info("max value is " + maxOfThreeNumbers.apply(a, b, c));
      logger.info("average value is " + averageOfThreeNumbers.apply(a, b, c));
    } catch (InputMismatchException e) {
      logger.error(WRONG_INPUT_MESSAGE);
    }

  }
  private static int readValue() {
    int value = scanner.nextInt();
    scanner.nextLine();
    return value;
  }
}
